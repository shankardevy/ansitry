defmodule Ansitry.PageController do
  use Ansitry.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
